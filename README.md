# Geração das assinaturas no diploma digital utilizando certificado em nuvem

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java.

### Formas de acesso ao Certificado em Nuvem

* *BRy KMS*: Certificado está armazenado na Plataforma de Certificados em Nuvem da BRy Tecnologia. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **PIN** ou **TOKEN**. Para credenciais com tipo **PIN** o Valor da Credencial corresponde a senha de acesso ao certificado em nuvem, enquanto que para credenciais do tipo **TOKEN** o Valor da Credencial corresponde a um token de autorização do acesso ao certificado. Mais informações sobre o serviço de certificados em nuvem podem ser encontradas em nossa documentação da [API de Certificado em Nuvem](https://api-assinatura.bry.com.br/api-certificado-em-nuvem).

* *GovBR*: Certificado está armazenado na Plataforma de Assinatura Digital gov.br. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **GOVBR** e o Valor da Credencial corresponde ao token de acesso do usuário. É possível verificar um exemplo da geração deste token de acesso em nossa [API Autenticação GOV.BR](https://gitlab.com/brytecnologia-team/integracao/api-autenticacao-token-govbr/react/gestao-credenciais).

* *HSM Dinamo*: Certificado está armazenado em um HSM Dinamo. Para este modo de acesso ao certificado o Tipo da Credencial selecionado deve ser **DINAMO** e o Valor da Credencial corresponde ao token de acesso, PIN da conta, ou OTP do usuário. É possível verificar um exemplo de como usar as credenciais do DINAMO em nosso [Repositório Swagger da API de Assinatura](https://hub2.bry.com.br/swagger-ui/index.html#/) no schema kms_data.
 
Este exemplo utiliza os seguintes assinantes para o diploma: 
- decano
- reitor
- a entidade emissora do diploma (IES Emissora)
- uma pessoa responsável pelo registro do diploma
- entidade registradora do diploma (IES Registradora)

Este exemplo está seperado em duas etapas: diploma público e diploma privado.

Os passos necessários para a geração das assinaturas no diploma público utilizando-se chave privada armazenada em nuvem:
  - Passo 1: Obtenção dos tokens de pré-autorização.
  - Passo 2: Assinatura dos responsáveis que normalmente assinam o diploma.
  - Passo 3: Assinatura da IES Emissora do diploma.
  - Passo 4: Assinatura dos responsáveis pelo registro do diploma.
  - Passo 5: Assinatura da IES Registradora do diploma.

Os passos necessários para a geração das assinaturas no diploma privado utilizando-se chave privada armazenada em nuvem:
  - Passo 1: Obtenção dos tokens de pré-autorização.
  - Passo 2: Assinatura da IES Emissora do diploma.
  - Passo 3: Assinatura da IES Registradora do diploma.
  
  
### Tech

O exemplo utiliza das bibliotecas Java abaixo:
* [SpringBoot Web] - Spring RestTemplate for create requests
* [JDK 8] - Java 8

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário incluir os certificados no nosso sistema em nuvem. Caso não tenha adicionado, [adicione-o](https://kms.bry.com.br/) ou [gere](https://kms.bry.com.br/) o certificado.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| JWT_TOKEN | Access Token para o consumo do serviço (JWT). | CredencialConfig
| UUID_REPRESENTANTE_EMISSORA | Identificador único do certificado do representante da IES emissora | KMSConfig
| UUID_EMISSORA | Identificador único do certificado da IES emissora | KMSConfig
| UUID_REPRESENTANTE_REGISTRADORA | Identificador único do certificado do representante da IES registradora | KMSConfig
| UUID_REGISTRADORA | Identificador único do certificado da IES registradora | KMSConfig
| PIN_REPRESENTANTE_EMISSORA | PIN do decano cadastrado no KMS, codificado em Base64 | KMSConfig
| PIN_REPRESENTANTE_EMISSORA | PIN do certificado do representante da emissora cadastrado no KMS | KMSConfig
| PIN_EMISSORA | PIN do certificado da emissora cadastrado no KMS | KMSConfig
| PIN_REPRESENTANTE_REGISTRADORA | PIN do certificado do representante da registradora cadastrado no KMS | KMSConfig
| PIN_REGISTRADORA | PIN do certificado da registradora cadastrado no KMS | KMSConfig


## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste num arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, compile diretamente as classes ou importe o projeto em sua IDE de preferência.
Utilizamos o JRE versão 8 para desenvolvimento e execução.

[SpringBoot Web]: <https://spring.io/>
[JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>
