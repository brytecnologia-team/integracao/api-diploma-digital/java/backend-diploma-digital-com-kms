# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-02-28

### Added

- Exemplo de geração de diploma digital com certificado em nuvem. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/backend-diploma-digital-com-kms/-/tags/1.0.0
