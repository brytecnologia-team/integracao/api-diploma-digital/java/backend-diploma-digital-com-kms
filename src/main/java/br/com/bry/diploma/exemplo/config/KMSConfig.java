package br.com.bry.diploma.exemplo.config;

public class KMSConfig {
	
	//Identificador do certificado dos assinantes
	public static final String UUID_REPRESENTANTE_EMISSORA = "<UUID_REPRESENTANTE_EMISSORA>";
	public static final String UUID_EMISSORA = "<UUID_EMISSORA>";
	public static final String UUID_REPRESENTANTE_REGISTRADORA = "<UUID_REPRESENTANTE_REGISTRADORA>";
	public static final String UUID_REGISTRADORA = "<UUID_REGISTRADORA>";

	//PIN dos assinantes configurados no BRyKMS
	public static final String PIN_REPRESENTANTE_EMISSORA = "<PIN_REPRESENTANTE_EMISSORA>";
	public static final String PIN_EMISSORA = "<PIN_EMISSORA>";
	public static final String PIN_REPRESENTANTE_REGISTRADORA = "<PIN_REPRESENTANTE_REGISTRADORA>";
	public static final String PIN_REGISTRADORA = "<PIN_REGISTRADORA>";

	//Lembrando que existem outros tipos de credenciais que podem ser usadas aqui (Caso utilize HSM DINAMO)
	//Veja mais sobre os parâmetros de credencial que vão em kms_data em nossa documentação
	//https://hub2.bry.com.br/swagger-ui/index.html#/ no schema 'kms_data' no fim da página
}
