package br.com.bry.diploma.exemplo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.bry.diploma.exemplo.config.CredencialConfig;
import br.com.bry.diploma.exemplo.config.KMSConfig;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AssinaturaDiplomaDigital {

	public static void main(final String[] args)
			throws IOException, ParserConfigurationException, SAXException,
			TransformerFactoryConfigurationError, TransformerException {
		//		Diploma d = new Diploma();
		//Carrega o XML do Diplomado e o XML de Documentação Acadêmica diplomas como InputStream.
		InputStream streamXMLDiplomado = new FileInputStream(new File("./src/main/resources/exemplo-xml-diploma-digital.xml"));
		InputStream streamXMLDocumentacaoAcademica = new FileInputStream(new File("./src/main/resources/exemplo-xml-documentacao.xml"));

		List<InputStream> xMLsDiplomado = new ArrayList<>();
		xMLsDiplomado.add(streamXMLDiplomado);

		List<InputStream> xMLsDocumentacaoAcademica = new ArrayList<>();
		xMLsDocumentacaoAcademica.add(streamXMLDocumentacaoAcademica);

		// Geração das assinaturas nos Diplomas XML de Documentação Acadêmica
		List<byte[]> diplomasPrivadosAssinados = assinarXMLDocumentacaoAcademica(xMLsDocumentacaoAcademica, CredencialConfig.JWT_TOKEN,
				KMSConfig.PIN_REPRESENTANTE_EMISSORA, KMSConfig.UUID_REPRESENTANTE_EMISSORA, KMSConfig.PIN_EMISSORA, KMSConfig.UUID_REGISTRADORA);
		
		ArrayList<InputStream> documentacaoAcademicaAssinadaAsStream = new ArrayList<InputStream>();
		for (byte[] documentacaoAcademicaAsBytes : diplomasPrivadosAssinados) {
			documentacaoAcademicaAssinadaAsStream.add(new ByteArrayInputStream(documentacaoAcademicaAsBytes));
		}
		
		List<byte[]> xMLsDiplomadosCompletos = copiaNodo(documentacaoAcademicaAssinadaAsStream, xMLsDiplomado);
		
		ArrayList<InputStream> xMLsDiplomadosCompletosAsStream = new ArrayList<InputStream>();
		for (byte[] xMLDiplomadosCompleto : xMLsDiplomadosCompletos) {
			xMLsDiplomadosCompletosAsStream.add(new ByteArrayInputStream(xMLDiplomadosCompleto));
		}
		// Geração das assinaturas nos Diplomas XML do Diplomado
		List<byte[]> diplomasPublicosAssinados = assinarXMLDiplomado(xMLsDiplomadosCompletosAsStream, CredencialConfig.JWT_TOKEN,
				KMSConfig.PIN_REPRESENTANTE_REGISTRADORA, KMSConfig.UUID_REPRESENTANTE_REGISTRADORA, KMSConfig.PIN_REGISTRADORA,
				KMSConfig.UUID_REGISTRADORA);

		//Salva os diplomasPublicosAssinados de alguma forma. Aqui está sendo salvo em disco.
		for (int i = 0; i < diplomasPublicosAssinados.size(); i++) {
			Files.write(Paths.get("Diploma-" + i + ".xml"), diplomasPublicosAssinados.get(i));
		}

//		Salva os diplomasPrivadosAssinados de alguma forma. Aqui está sendo salvo em disco.
		for (int i = 0; i < diplomasPrivadosAssinados.size(); i++) {
			Files.write(Paths.get("DocumentacaoAcademica-" + i + ".xml"), diplomasPrivadosAssinados.get(i));
		}

	}
	
	private static List<byte[]> assinarXMLDocumentacaoAcademica(List<InputStream> documentacoesAcademicas, String tokenFrameWork,
			String kmsCredencialRepresentante, String uuidRepresentante, String kmsCredencialIESEmissora, String uuidIESEmissora) {

		try {
			String[] assinados = null;
			List<InputStream> assinadosConvertidos = new ArrayList<>();

			kmsCredencialRepresentante = Base64.getEncoder().encodeToString(kmsCredencialRepresentante.getBytes());
			kmsCredencialIESEmissora = Base64.getEncoder().encodeToString(kmsCredencialIESEmissora.getBytes());

			// Este XML possui sempre três assinaturas. As duas primeiras que assinam o nodo DadosDiploma
			// e a terceira que assina o documento inteiro, inclusive a assinatura anterior.

			//Inicialmente é assinado o nodo DadosDiploma com o certificado (e-CNPJ) da IES Emissora (utilizando o token gerado anteriormente)
			assinados = gerarAssinaturaXML("RepresentanteEmissora", tokenFrameWork, kmsCredencialRepresentante,
					uuidRepresentante, documentacoesAcademicas);
			assinadosConvertidos = new ArrayList<>();
			for (String s : assinados) {
				assinadosConvertidos.add(new ByteArrayInputStream(Base64.getDecoder().decode(s)));
			}

			assinados = gerarAssinaturaXML("IESEmissoraDadosDiploma", tokenFrameWork, kmsCredencialIESEmissora,
					uuidIESEmissora, assinadosConvertidos);
			assinadosConvertidos = new ArrayList<>();
			for (String s : assinados) {
				assinadosConvertidos.add(new ByteArrayInputStream(Base64.getDecoder().decode(s)));
			}
			
			assinados = gerarAssinaturaXML("IESEmissoraRegistro", tokenFrameWork, kmsCredencialIESEmissora,
					uuidIESEmissora, assinadosConvertidos);
			assinadosConvertidos = new ArrayList<>();
			for (String s : assinados) {
				assinadosConvertidos.add(new ByteArrayInputStream(Base64.getDecoder().decode(s)));
			}
			
			List<byte[]> retornos = new ArrayList<>();
			for (String s : assinados) {
				retornos.add(Base64.getDecoder().decode(s));
			}
			return retornos;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static List<byte[]> assinarXMLDiplomado(List<InputStream> diplomas, String tokenFrameWork,
			String kmsCredencialRepresentanteRegistradora, String uuidRepresentanteRegistradora, String kmsCredencialRegistradora,
			String uuidRegistradora) {

		String[] assinados = null;
		try {

			//Pode ser interessante obter a pré-autorização no KMS (para não ser necessário inserir o PIN a cada assinatura)
			//Quantidade de assinaturas equivale a quantidade de assinaturas que o token vai poder assinar
			//Tempo é o tempo de expiração do token, em segundos
			//Mais informações em https://kms.bry.com.br/kms-api/?urls.primaryName=API%20KMS%20BRyCloud#/6.%20Pr%C3%A9-autoriza%C3%A7%C3%A3o/autorizar
			
			kmsCredencialRepresentanteRegistradora = Base64.getEncoder().encodeToString(kmsCredencialRepresentanteRegistradora.getBytes());
			kmsCredencialRegistradora = Base64.getEncoder().encodeToString(kmsCredencialRegistradora.getBytes());

			//Com os tokens, é iniciado o processo de geração das assinaturas. A ordem é importante.

			//Gera assinatura do decano
			assinados = gerarAssinaturaXML("RepresentanteRegistradora", tokenFrameWork, kmsCredencialRepresentanteRegistradora,
					uuidRepresentanteRegistradora, diplomas);
			List<InputStream> assinadosConvertidos = new ArrayList<>();
			for (String item : assinados) {
				assinadosConvertidos.add(new ByteArrayInputStream(Base64.getDecoder().decode(item)));
			}
			//Gera assinatura do reitor
			assinados = gerarAssinaturaXML("Registradora", tokenFrameWork, kmsCredencialRegistradora,
					uuidRegistradora, assinadosConvertidos);
			assinadosConvertidos = new ArrayList<>();
			for (String value : assinados) {
				assinadosConvertidos.add(new ByteArrayInputStream(Base64.getDecoder().decode(value)));
			}

			//Converte para byte array
			List<byte[]> retornos = new ArrayList<>();
			for (String s : assinados) {
				retornos.add(Base64.getDecoder().decode(s));
			}
			return retornos;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String[] gerarAssinaturaXML(String tipoAssinatura, String tokenFW,
			String credencialKMS, String uuid, List<InputStream> diplomas)
					throws IOException {
		
		JSONObject kmsData = new JSONObject();
		
		// Tipo de sistema de hospedagem de certificados - Available values : BRYKMS, DINAMO
		String kms_type = "BRYKMS";
		
		//UUID do Certificado no KMS BRy
		kmsData.put("uuid_cert", uuid);
		//PIN do compartimento de certificados
		kmsData.put("pin", credencialKMS);
		//Token de Pré-Autorização (ou Token DINAMO)
		//kmsData.put("token", tokenAutorizacao);
		
		//Usuario (CPF - opcional / Usuario DINAMO)
		kmsData.put("user", credencialKMS);
		
		//Credenciais DINAMO, caso use HSM Dinamo
		//kmsData.put("uuid_pkey", uuidPkey);
		//kmsData.put("otp", otp);
		
		RequestSpecification form = RestAssured.given()
				.header("Authorization", "Bearer " + tokenFW)
				.header("kms_type", kms_type) 
				.header("Content-Type", "multipart/form-data")
		        .multiPart("nonce", "1")
		        .multiPart("signatureFormat", "ENVELOPED")
		        .multiPart("hashAlgorithm", "SHA256")
				.multiPart("returnType", "BASE64")
				.multiPart("kms_data", kmsData.toString()); 

		for (int i = 0; i < diplomas.size(); i++) {

			form.multiPart("originalDocuments[" + i + "][content]", "arquivo.xml", diplomas.get(i));

			switch (tipoAssinatura) {
			case "RepresentanteEmissora":
				//PF
				form.multiPart("originalDocuments[" + i + "][specificNode][name]", "DadosDiploma");
				form.multiPart("originalDocuments[" + i + "][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");
				form.multiPart("profile", "ADRC");
				break;
			case "IESEmissoraDadosDiploma":
				// PJ
				form.multiPart("originalDocuments[" + i + "][specificNode][name]", "DadosDiploma");
				form.multiPart("originalDocuments[" + i + "][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");
				form.multiPart("profile", "ADRC");
				form.multiPart("includeXPathEnveloped", "false");

				break;
			case "IESEmissoraRegistro":
				// PJ
				form.multiPart("profile", "ADRA");
				form.multiPart("includeXPathEnveloped", "false");

				break;
			case "RepresentanteRegistradora":
				// PF
				form.multiPart("originalDocuments[" + i + "][specificNode][name]", "DadosRegistro");
				form.multiPart("originalDocuments[" + i + "][specificNode][namespace]", "http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd");
				form.multiPart("profile", "ADRC");

				break;
			case "Registradora":
				// PJ
				form.multiPart("profile", "ADRA");
				form.multiPart("includeXPathEnveloped", "false");

				break;
			}
		}
		Response response = null;
		try {
			response = form.post("https://diploma.hom.bry.com.br/api/xml-signature-service/v1/signatures/kms");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.body().as(String[].class);
	}
	
	private static List<byte[]> copiaNodo(List<InputStream> documentacaoAcademica, List<InputStream> xMLDiplomado)
			throws IOException, ParserConfigurationException, SAXException,
			TransformerFactoryConfigurationError, TransformerException {
		
		List<byte[]> listaDiplomasCompletos = new ArrayList<byte[]>();
		
		for (int i = 0; i < documentacaoAcademica.size(); i++) {
			
			InputStream inputStream = documentacaoAcademica.get(i);
			File tempFile = new File("./src/main/resources/.temp/temp_documentacaoAcademica.xml");
			if(tempFile.exists()) {
				tempFile.delete();
				if(!tempFile.createNewFile())
					throw new RuntimeException("Falha ao criar arquivo temporario .temp/temp_documentacaoAcademica.xml");
			}
			FileOutputStream outputStream = new FileOutputStream(tempFile);
			byte[] buffer = new byte[2048];
			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			outputStream.close();
			
			InputStream inputStream2 = xMLDiplomado.get(i);
			File tempFile2 = new File("./src/main/resources/.temp/temp_XMLDiplomado.xml");
			if(tempFile2.exists()) {
				tempFile2.delete();
				if(!tempFile2.createNewFile())
					throw new RuntimeException("Falha ao criar arquivo temporario no diretorio .temp/temp_XMLDiplomado.xml");
			}
			FileOutputStream outputStream2 = new FileOutputStream(tempFile2);
			buffer = new byte[2048];
			while ((bytesRead = inputStream2.read(buffer)) != -1) {
				outputStream2.write(buffer, 0, bytesRead);
			}
			outputStream2.close();
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			Document doc = db.parse(new FileInputStream("./src/main/resources/.temp/temp_documentacaoAcademica.xml"));
			doc.getDocumentElement().normalize();
			
			Node registroReqNode = doc.getElementsByTagName("RegistroReq").item(0);
			Element registroReq = (Element) registroReqNode;
			
			NodeList dadosDiplomaList = registroReq.getElementsByTagName("DadosDiploma");
			if (dadosDiplomaList.getLength() == 0) {
				dadosDiplomaList = registroReq.getElementsByTagName("DadosDiplomaNSF");
			}
			Node dadosDiplomaNode = dadosDiplomaList.item(0);
							
			DocumentBuilder dbDiplomado = dbf.newDocumentBuilder();
			Document docDiplomado = dbDiplomado.parse(new FileInputStream("./src/main/resources/.temp/temp_XMLDiplomado.xml"));
			
			docDiplomado.getDocumentElement().normalize();
			
			Node infDiplomaNode = docDiplomado.getElementsByTagName("infDiploma").item(0);
			Element infDiploma = (Element) infDiplomaNode;
			
			infDiploma.insertBefore(docDiplomado.adoptNode(dadosDiplomaNode.cloneNode(true)), infDiploma.getFirstChild());
			
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			Result output = new StreamResult(new File("./src/main/resources/.temp/XMLDiplomadoCompleto.xml"));
			Source input = new DOMSource(docDiplomado);
			transformer.transform(input, output);
			
			File file = new File("./src/main/resources/.temp/XMLDiplomadoCompleto.xml");
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			buffer = new byte[2048];
			while ((bytesRead = fis.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}
			fis.close();
			
			tempFile.delete();
			tempFile2.delete();
			file.delete();
			
			byte[] xMLDiplomadoCompleto = baos.toByteArray();
			listaDiplomasCompletos.add(xMLDiplomadoCompleto);
			
		}
		return listaDiplomasCompletos;
	}
}
